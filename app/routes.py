from flask import render_template, request, redirect
from app.detect_face import detect_faces
from app import app, db
from app.models import Product, Celebrity
from flask import Flask,abort,render_template,request,redirect,url_for
from werkzeug import secure_filename
import os
from random import randint
import cv2
import sys
import time
import colorsys
from skimage import io

uploads_dir = os.path.join(app.instance_path, '')


@app.route('/')
def index():
    return render_template('licemer.html')

@app.route('/products')
def products():
    products = Product.query.all()
    return render_template('products.html', products=products)

@app.route('/celebrities')
def celebrities():
    celebrities = Celebrity.query.all()
    return render_template('celebrities.html', celebrities=celebrities)

@app.route('/new_product', methods=['POST'])
def new_product():
    if request.method == 'POST':
        form = request.form
        color = form.get('color')
        url = form.get('url')
        sex = form.get('sex')
        product = Product(color = color, url = url, sex = sex)
        db.session.add(product)
        db.session.commit()
        return redirect('/products')

@app.route('/new_celebrity', methods=['POST'])
def new_celebrity():
    if request.method == 'POST':
        form = request.form
        face = form.get('face')
        clothes = form.get('clothes')
        celebrity = Celebrity(face = face, clothes = clothes)
        db.session.add(celebrity)
        db.session.commit()
        return redirect('/celebrities')

@app.route('/delete_product/<int:product_id>')
def delete_product(product_id):
    product = Product.query.get(product_id)
    db.session.delete(product)
    db.session.commit()
    return redirect('/products')

@app.route('/delete_celebrity/<int:celebrity_id>')
def delete_celebrity(celebrity_id):
    celebrity = Celebrity.query.get(celebrity_id)
    db.session.delete(celebrity)
    db.session.commit()
    return redirect('/celebrities')

@app.route('/upload', methods = ['POST'])
def upload_file():
    f = request.files['file']
    filename = os.path.join(uploads_dir, 'photo.jpg')
    time.sleep(5)
    f.save(filename)
    h = detect_faces(filename)
    return h




@app.route('/+<float:color>')
def recomend(color):
    return str(color)
